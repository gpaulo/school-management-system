package dbUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class dbConnection {
  private static final String USERNAME = "root";
  private static final String PASSWRD = "";
  private static final String DRIVERMySql = "com.mysql.jdbc.Driver";
  private static final String URLMYSQL = "jdbc:mysql://localhost/escuela";
  //private static final String DRIVERSqlite = "org.sqlite.JDBC";
  //private static final String SQLCON = "jdbc:sqlite:escuelaLogin.sqlite";
  
  public static Connection getConnection() throws SQLException {
    try {
      Class.forName(DRIVERMySql);
      return DriverManager.getConnection(URLMYSQL,USERNAME,PASSWRD);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    return null;
  }
}
