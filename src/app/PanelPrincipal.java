
package app;

/**
 *
 * @author ubuntu
 */
public class PanelPrincipal extends javax.swing.JFrame {
  
  /**
   * Creates new form PanelPrincipal
   */
  public PanelPrincipal() {
    this.setTitle("Panel Principal de la Escuela");
    initComponents();
    this.setResizable(false);
    this.setLocationRelativeTo(null);
  }


  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    btnGestionarAlumnos = new javax.swing.JButton();
    btnGestionarMaterias = new javax.swing.JButton();
    jLabel1 = new javax.swing.JLabel();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

    btnGestionarAlumnos.setText("Gestionar Alumnos");
    btnGestionarAlumnos.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnGestionarAlumnosActionPerformed(evt);
      }
    });

    btnGestionarMaterias.setText("Gestionar Materias");
    btnGestionarMaterias.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnGestionarMateriasActionPerformed(evt);
      }
    });

    jLabel1.setText("Panel Principal");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addGap(24, 24, 24)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jLabel1)
          .addComponent(btnGestionarMaterias)
          .addComponent(btnGestionarAlumnos))
        .addGap(0, 406, Short.MAX_VALUE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(jLabel1)
        .addGap(76, 76, 76)
        .addComponent(btnGestionarAlumnos)
        .addGap(38, 38, 38)
        .addComponent(btnGestionarMaterias)
        .addContainerGap(109, Short.MAX_VALUE))
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void btnGestionarAlumnosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGestionarAlumnosActionPerformed
    LimitePersona lPersona = new LimitePersona();
    lPersona.setVisible(true);
    this.setVisible(false);
  }//GEN-LAST:event_btnGestionarAlumnosActionPerformed

  private void btnGestionarMateriasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGestionarMateriasActionPerformed
    LimiteMateria lMaterias = new LimiteMateria();
    lMaterias.setVisible(true);
    this.setVisible(false);
  }//GEN-LAST:event_btnGestionarMateriasActionPerformed
  

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnGestionarAlumnos;
  private javax.swing.JButton btnGestionarMaterias;
  private javax.swing.JLabel jLabel1;
  // End of variables declaration//GEN-END:variables
}
