package app;

/**
 * @author Paulo, Iván
 */
import com.mysql.jdbc.Connection;

import java.awt.HeadlessException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import dbUtil.dbConnection;

public class ControlCRUDMaterias {

  TableRowSorter nameFilter;
  PreparedStatement ps;
  ResultSet rs;
  ResultSetMetaData rsmd;
  DefaultTableModel tm;
  Connection con = null;
  String sql;

  public void agrega_materia() {
    try {
      con = (Connection) dbConnection.getConnection();
      sql = "INSERT INTO `materias` (`descripcion`, `clave_materia`, `anio`, `duracion`, IDF_carrera, estado) VALUES (?, ?, ?, ?, 0, '')";
      ps = con.prepareStatement(sql);
      ps.setString(1, LimiteMateria.txtNombreMateria.getText());
      ps.setString(2, LimiteMateria.txtClaveMateria.getText());
      ps.setString(3, LimiteMateria.txtAnioMateria.getText());
      ps.setString(4, LimiteMateria.selecDuracionMateria.getSelectedItem().toString());

      int res = ps.executeUpdate();

      if (res > 0) {
        JOptionPane.showMessageDialog(null, "Materia Agregada");
        limpiar_campos();
      } else {
        JOptionPane.showMessageDialog(null, "Error al agregar la materia");
        limpiar_campos();
      }

      con.close();
    } catch (HeadlessException | SQLException e) {
      System.err.println(e);
    }
  }

  public void muestra_materia() {
    if (!"".equals(LimiteMateria.txtBuscar.getText())) {
      JOptionPane.showMessageDialog(null, "Borrar el contenido del campo buscar.");
      return;
    }

    try {
      con = (Connection) dbConnection.getConnection();
      sql = "SELECT `ID_materias`, `descripcion`, `clave_materia`, `anio`, `duracion` FROM `materias`";
      ps = con.prepareStatement(sql);
      rs = ps.executeQuery();
      rsmd = rs.getMetaData();
      int colSize = rsmd.getColumnCount();

      Utilidades.limpiarTabla(LimiteMateria.TablaMaterias);
      while (rs.next()) {
        Object[] row = new Object[colSize];

        for (short elem = 0; elem < colSize; elem++) {
          row[elem] = rs.getObject(elem + 1);
        }

        ((DefaultTableModel) LimiteMateria.TablaMaterias.getModel()).addRow(row);
      }

    } catch (HeadlessException | SQLException ex) {
      System.err.println(ex);
    }
  }

  public void actuliza_materia() {
    try {
      con = (Connection) dbConnection.getConnection();
      sql = "UPDATE materias SET descripcion=?, clave_materia=?, anio=?, duracion=? WHERE ID_materias=?";
      ps = con.prepareStatement(sql);
      ps.setString(1, LimiteMateria.txtNombreMateria.getText());
      ps.setString(2, LimiteMateria.txtClaveMateria.getText());
      ps.setString(3, LimiteMateria.txtAnioMateria.getText());
      ps.setString(4, LimiteMateria.selecDuracionMateria.getSelectedItem().toString());
      ps.setInt(5, Integer.parseInt(LimiteMateria.txtIdMateria.getText()));

      int res = ps.executeUpdate();

      if (res > 0) {
        JOptionPane.showMessageDialog(null, "Materia Modificada");
        limpiar_campos();
      } else {
        JOptionPane.showMessageDialog(null, "Error al Modificar materia");
        limpiar_campos();
      }

      con.close();

    } catch (HeadlessException | SQLException e) {
      System.err.println(e);
    }
  }

  public void remueve_materia() {
    try {
      con = (Connection) dbConnection.getConnection();
      sql = "DELETE FROM materias WHERE ID_materias=?";
      ps = con.prepareStatement(sql);
      ps.setInt(1, Integer.parseInt(LimiteMateria.txtIdMateria.getText()));

      int res = ps.executeUpdate();

      if (res > 0) {
        JOptionPane.showMessageDialog(null, "Materia Eliminada");
        limpiar_campos();
      } else {
        JOptionPane.showMessageDialog(null, "Error al eliminar la materia");
        limpiar_campos();
      }

      con.close();

    } catch (HeadlessException | SQLException e) {
      System.err.println(e);
    }
  }

  public void busca_materia() {
    if (LimiteMateria.TablaMaterias.getRowCount() != 0) {
      LimiteMateria.txtBuscar.addKeyListener(new KeyAdapter() {
        @Override
        public void keyReleased(final KeyEvent e) {
          int colToFind = 1;
          if (LimiteMateria.selecBuscar.getSelectedItem() == "Clave") {
            colToFind++;
          }
          nameFilter.setRowFilter(RowFilter.regexFilter(LimiteMateria.txtBuscar.getText(), colToFind));
        }
      });
      nameFilter = new TableRowSorter(LimiteMateria.TablaMaterias.getModel());
      LimiteMateria.TablaMaterias.setRowSorter(nameFilter);
    }
  }

  private void limpiar_campos() {
    LimiteMateria.txtNombreMateria.setText("");
    LimiteMateria.txtClaveMateria.setText("");
    LimiteMateria.txtAnioMateria.setText("");
    LimiteMateria.selecDuracionMateria.setSelectedIndex(0);
    LimiteMateria.txtIdMateria.setText("");
  }

  public void tabla_a_campos_materias() {
    int rec = LimiteMateria.TablaMaterias.getSelectedRow();
    LimiteMateria.txtIdMateria.setText(LimiteMateria.TablaMaterias.getValueAt(rec, 0).toString());
    LimiteMateria.txtNombreMateria.setText(LimiteMateria.TablaMaterias.getValueAt(rec, 1).toString());
    LimiteMateria.txtClaveMateria.setText(LimiteMateria.TablaMaterias.getValueAt(rec, 2).toString());
    LimiteMateria.txtAnioMateria.setText(LimiteMateria.TablaMaterias.getValueAt(rec, 3).toString());
    LimiteMateria.selecDuracionMateria.setSelectedItem(LimiteMateria.TablaMaterias.getValueAt(rec, 4).toString());
  }
}
